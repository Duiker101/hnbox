var localStorageKeyHistory = "hnhistory";
var storage = getStorage(localStorageKeyHistory);
var overlay,popUp;

if(!storage.history){
	storage.history = [];
}

var aExp = $("<a> [H]</a>");
aExp.click(function(){
	showHistory(this);
});

$(".pagetop a:last").after(aExp);

function showHistory(anchor){
	var pos= $(anchor).offset();

	if(!overlay){
		overlay = $("<div class='historyOverlay'></div>");
		popUp = $("<div class='historyPopUp'></div>");
		overlay.append(popUp);
		popUp.append("<div class='tipHead'>History</div>");
		popUp.append("<div class='tipBody'></div>");
		$("body").append(overlay);
	}

	var body = popUp.find(".tipBody");
	body.empty();

	for(var i in storage.history){
		var entry = storage.history[i];	
		var link = $("<a></a>");
		var clink = $("<a></a>");

		link.text(entry.title);
		clink.text(" [C]");

		link.attr("href",entry.link);
		clink.attr("href",entry.clink);

		body.prepend("<br />");
		body.prepend(clink);
		body.prepend(link);
	}

	var close = $("<input type='button' value='Close' />");
	body.append(close);

	var reset = $("<input type='button' value='Reset' />");
	body.append(reset);

	overlay.show();
	
	reset.click(function(){
		storage.history = [];
		saveStorage(localStorageKeyHistory,storage);
	});

	close.click(function(){
		overlay.hide();
	});
}


$("td.title a").click(function(){
	var clink =$(this).parent().parent().next().find("td.subtext a:last").attr("href");

	if(storage.history.length > 20){
		storage.history.splice(0,1);
	}

	storage.history.push({title:$(this).text(), link:$(this).attr("href"),clink: clink, type:"link"});

	saveStorage(localStorageKeyHistory,storage);
	console.log(storage);
});
