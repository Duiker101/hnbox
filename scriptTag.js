var localStorageKeyTag = "hntag";
var storageTag = getStorage(localStorageKeyTag);
var tagFields=["Tag","Color","Link"];

genTags();
genExpButton();

function genExpButton(){
	var aExp = $("<a> [+]</a>");
	aExp.click(function(){
		genExpPopUp(this);
	});
	$(".pagetop a:last").after(aExp);
}
function genExpPopUp(anchor){
	$("#tooltip").remove();
	var pos= $(anchor).offset();
	$("body").append("<div id='tooltip' class='tooltip'></div>");
	$("#tooltip").append("<div id='tipHead' class='tipHead'>Import/Export</div>");
	$("#tooltip").append("<div id='tipBody' class='tipBody'></div>");
	$("#tipBody").append("<textarea id='expTextArea' rows=10 cols=30/><br />");
	$("#expTextArea").text(JSON.stringify(storageTag));
	$("#tipBody").append("<input type='button' id='importTags' value='Import' />");
	$("#tipBody").append("<input type='button' value='Close' id='closeExport' />");
	$("#tooltip").css("left",pos.left-250);
	$("#tooltip").css("top",pos.top+15);
	$("#tooltip").show();
	
	$("#importTags").click(function(){
		try{
			var newjson=JSON.parse($("#expTextArea").val());
			storageTag = newjson;
			saveStorage(localStorageKeyTag,storageTag);
			$("#tooltip").remove();
			removeOldTags();
			genTags();
		}catch(ex){
			alert("Invalid JSON ("+ex.message+")" );
		}
	});
	
	$("#closeExport").click(function(){
		$("#tooltip").remove();
	});
}

function genTags(){
	storageTag = getStorage(localStorageKeyTag);
	$("a[href^=user]").each(function(){
		if($(this).parent(".pagetop").length > 0)
			return;
		
		var tagLink = $("<a />");
		var localValue= storageTag[$(this).text()];
		var tag = {};
		var thisAnchor = this;
		
		for(var i in tagFields){
			tag[tagFields[i]]=""
		}
		
		if(localValue != undefined){
			var tagJSON = localValue;
			for(var i in tagJSON){
				tag[i] = tagJSON[i];
			}
		}
		
		tagLink.click(function(e){
			genTooltip(thisAnchor,tag);
		});
		
		tagLink.text(" [ " + tag["Tag"] + " ]");
		tagLink.css("color",tag["Color"]);
		tagLink.css("cursor","pointer");
		
		$(this).after(tagLink);
	
	});
}

function genTooltip(anchor,tag){
	$("#tooltip").remove();
	var pos= $(anchor).offset();
	var tagName= $(anchor).text();
	
	$("body").append("<div id='tooltip' class='tooltip'></div>");
	$("#tooltip").append("<div id='tipHead' class='tipHead'>Add a new Tag</div>");
	$("#tooltip").append("<div id='tipBody' class='tipBody'></div>");
	
	var j=0;
	var tagC = 0;
	for(var i in tag)
		tagC++;
	
	for(var i in tag){
		if(j <= tagFields.length - 1){
			$("#tipBody").append("<input id='tagInput"+i+"' data-name='"+i+"' placeHolder='"+i+"' value='"+tag[i]+"'/>");
		}else{
			var exNum =i.replace(/extra/,"");
			$("#tipBody").append("<input id='tagInputExtra"+exNum+"' data-extra='"+exNum+"' data-name='"+i+"' value='"+tag[i]+"' />");
		}
		if(j <= tagC - 2)
			$("#tipBody").append("<br />");
		j++;
	}
	
	if($("#tagInputColor").val() == "")
		$("#tagInputColor").val("#0AA");
		
	$("#tipBody").append("<a id='addExtraField'>+</a><br />");
	$("#tipBody").append("<input type='button' id='tagSave' value='Save' />");
	$("#tipBody").append("<input type='button' value='Cancel' id='tagCancel' />");
	$("#tipBody").append("<input type='hidden' id='tagName' />");
	
	$("#tagName").val(tagName);
	$("#tooltip").css("left",pos.left);
	$("#tooltip").css("top",pos.top);
	$("#tooltip").show();
	
	bindTooltip(anchor,tag);
}

function bindTooltip(anchor,tag){

	$("#addExtraField").click(function(){
		addExtraField('');
	});
	
	$("#tagCancel").bind("click",function(){
		$("#tooltip").remove();
	});
	
	$("#tagSave").bind("click",function(){
		var tagNew={};
		
		$("input[id^=tagInput]").each(function(){
			if($(this).val() != "")
				tagNew[$(this).data("name")] = $(this).val();
		});	
		
		storageTag[$("#tagName").val()]=tagNew;
		
		$("#tooltip").remove();
		removeOldTags();
		saveStorage(localStorageKeyTag,storageTag);
		genTags();
	});
}

function removeOldTags(){
	$("a[href^=user]").each(function(){
		if($(this).parent(".pagetop").length > 0)
			return;
		$(this).next("a").remove();
	});
}

function addExtraField(value){
	var last = $("input[id^=tagInputExtra]:last");
	var lastNum = 0;
	if(last.length > 0)
		lastNum = parseInt(last.data("extra"))+1;
	$("input[id^=tagInput]:last").after("<br /><input id='tagInputExtra"+lastNum+"' data-extra='"+lastNum+"' data-name='extra"+lastNum+"' value='"+value+"' />");
}
 
 
